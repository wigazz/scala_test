package dao

import models.ProductTable

import scala.concurrent.{Future, ExecutionContext}
import slick.driver.PostgresDriver.api._

class ProductDAO()(implicit ec: ExecutionContext) {

  private val products = TableQuery[ProductTable]

  def findByStoreId(storeId: Int): DBIO[Seq[models.Product]] = products.filter(_.storeId === storeId).result

  def findById(productId: Int): DBIO[Option[models.Product]] = products.filter(_.id === productId).result.headOption

  def all(): DBIO[Seq[models.Product]] =products.result

  def insert(product: models.Product): DBIO[models.Product] =  ( ( products returning products.map(_.getFields) ) += product ).map(fields => (models.Product.apply _).tupled(fields))

}
