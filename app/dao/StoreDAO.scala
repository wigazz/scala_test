package dao

import models.{StoreWithProducts, ProductTable, Store, StoreTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}

class StoreDAO()(implicit ec: ExecutionContext) {

  private val stores = TableQuery[StoreTable]

  def all(): DBIO[Seq[Store]] = stores.result

  def findById(storeId: Int): DBIO[Option[Store]] = stores.filter(_.id === storeId).take(1).result.headOption

//  def findByIdWithProducts(storeId: Int): DBIO[Option[StoreWithProducts]] = {
//    findById(storeId).flatMap({
//      case None => Future(None)
//      case Some(store) => productDAO.findByStoreId(store.id.get).map(productsSeq => Some(StoreWithProducts(store, productsSeq)))
//    })
//  }

  def findByName(name: String): DBIO[Option[Store]] = stores.filter(_.name === name).take(1).result.headOption

  def insert(store: Store): DBIO[Store] =  ( ( stores returning stores.map(_.getFields) ) += store ).map(fields => (Store.apply _).tupled(fields))

}
