package dao

import models.{OrderItem, OrderItemTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}


class OrderItemDAO(orderDAO: OrderDAO, productDAO: ProductDAO)(implicit ec: ExecutionContext) {

  private val orderItems = TableQuery[OrderItemTable]

  def all(): DBIO[Seq[OrderItem]] = orderItems.result

  def findByOrderIdAndProductId(orderId: Int, productId: Int): DBIO[Option[OrderItem]] = orderItems.filter(orderItem => orderItem.orderId === orderId && orderItem.productId === productId).take(1).result.headOption

  def updateQuantity(orderItem: OrderItem, quantity: Int): DBIO[Int] = {
    (for {
      oi <- orderItems if oi.id === orderItem.id.get
    } yield oi.quantity).update(quantity)
  }

  def insert(orderItem: OrderItem): DBIO[OrderItem] = ( ( orderItems returning orderItems.map(_.getFields) ) += orderItem ).map(fields => (OrderItem.apply _).tupled(fields))

}
