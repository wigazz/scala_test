package dao

import models.{UsersTable, User}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}


class UserDAO()(implicit ec: ExecutionContext) {

  private val users = TableQuery[UsersTable]

  def all(): DBIO[Seq[User]] = users.result

  def findByLogin(login: String): DBIO[Option[User]] = users.filter(_.login === login).take(1).result.headOption

  def insert(user: User): DBIO[User] = ( ( users returning users.map(_.getFields) ) += user ).map(fields => (User.apply _).tupled(fields))

}
