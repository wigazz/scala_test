package dao

import models.{Order, OrderTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}

class OrderDAO()(implicit ec: ExecutionContext) {

  private val orders = TableQuery[OrderTable]

  def findOpenOrderByUserId(userId: Int): DBIO[Option[Order]] = orders.filter(order => order.userId === userId && order.status === "open" ).take(1).result.headOption

  def all(): DBIO[Seq[Order]] = orders.result

  def insert(order: Order): DBIO[Order] =  ( ( orders returning orders.map(_.getFields)) += order ).map(fields => (Order.apply _).tupled(fields))

}
