package models

import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._

case class Product(id:Option[Int] = None, name:String, price: Double, quantity: Int, storeId: Int)

object Product {
  implicit val format = Json.format[Product]
}

class ProductTable(tag: Tag) extends Table[Product](tag, "tbl_product") {

  private val stores = TableQuery[StoreTable]

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def price = column[Double]("price")
  def quantity = column[Int]("quantity")
  def storeId = column[Int]("store_id")

  def store = foreignKey("product_store_fk", storeId, stores)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)

  def getFields = (id.?, name, price, quantity, storeId)

  def * = getFields <> ((Product.apply _).tupled, Product.unapply)
}