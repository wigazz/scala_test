package models

import play.api.libs.json.Json
import shapeless.LabelledGeneric
import slick.driver.PostgresDriver.api._

import shapeless._
import syntax.singleton._
import record._

case class Store(id:Option[Int] = None, name:String)

object Store {
  implicit val format = Json.format[Store]
}


case class StoreWithProducts(id:Option[Int] = None, name:String, products: Seq[Product] = Seq())

object StoreWithProducts {

  def create(store: Store, products: Seq[Product] = Seq()) = {
    val storeGen = LabelledGeneric[Store]
    val storeWithProductsGen = LabelledGeneric[StoreWithProducts]

    storeWithProductsGen.from(storeGen.to(store) + ('products ->> products))
  }

  implicit val format = Json.format[StoreWithProducts]
}


class StoreTable(tag: Tag) extends Table[Store](tag, "tbl_store") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")

  def getFields = (id.?, name)

  def * = (id.?, name) <> ((Store.apply _).tupled, Store.unapply)
}