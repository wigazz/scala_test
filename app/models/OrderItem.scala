package models

import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._

case class OrderItem(id: Option[Int] = None, orderId: Int, productId: Int, quantity: Int)

object OrderItem
{
  implicit val format = Json.format[OrderItem]
}

class OrderItemTable(tag: Tag) extends Table[OrderItem](tag, "tbl_order_item") {

  private val orders = TableQuery[OrderTable]
  private val products = TableQuery[ProductTable]

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def orderId = column[Int]("order_id")
  def productId = column[Int]("product_id")
  def quantity = column[Int]("quantity")

  def order = foreignKey("order_item_order_fk", orderId, orders)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  def product = foreignKey("order_item_product_fk", productId, products)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)

  def getFields = (id.?, orderId, productId, quantity)

  def * = getFields <> ((OrderItem.apply _).tupled, OrderItem.unapply)
}