package models

import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._

case class Order(id:Option[Int] = None, userId: Int, status: String)

object Order {

  implicit val format = Json.format[Order]
}

class OrderTable(tag: Tag) extends Table[Order](tag, "tbl_order") {

  private val users = TableQuery[UsersTable]

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user_id")
  def status = column[String]("status")

  def user = foreignKey("order_user_fk", userId, users)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)

  def getFields = (id.?, userId, status)

  def * = getFields <> ((Order.apply _).tupled, Order.unapply)
}

