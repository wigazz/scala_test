package modules

import slick.driver.PostgresDriver.api._
import play.api.db._

trait DatabaseModule extends DBComponents with HikariCPComponents {
  def dbApi: DBApi
  lazy val db = Database.forDataSource(dbApi.database("default").dataSource)
}