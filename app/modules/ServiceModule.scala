package modules


import com.softwaremill.macwire._
import dao._
import services.{OrderService, StoreService, UserService}
import slick.driver.PostgresDriver.api._

import scala.concurrent.ExecutionContext

trait ServiceModule {

  implicit def ec: ExecutionContext
  def db: Database

  def userDao: UserDAO
  def storeDao: StoreDAO
  def productDao: ProductDAO
  def orderDao: OrderDAO
  def orderItemDao: OrderItemDAO

  lazy val userService = wire[UserService]
  lazy val storeService = wire[StoreService]
  lazy val orderService = wire[OrderService]

}
