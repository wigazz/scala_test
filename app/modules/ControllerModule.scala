package modules

import com.softwaremill.macwire._
import controllers.{OrderController, StoreController, UserController, ApplicationController}
import services.{OrderService, StoreService, UserService}

import scala.concurrent.ExecutionContext

trait ControllerModule {

  implicit def ec: ExecutionContext
  def userService: UserService
  def storeService: StoreService
  def orderService: OrderService

  lazy val applicationController = wire[ApplicationController]
  lazy val userController = wire[UserController]
  lazy val storeController = wire[StoreController]
  lazy val orderController = wire[OrderController]

}
