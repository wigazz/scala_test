package modules

import dao._
import slick.driver.PostgresDriver.api._
import com.softwaremill.macwire._

import scala.concurrent.ExecutionContext

trait DaoModule {

  implicit def ec: ExecutionContext

  lazy val userDao = wire[UserDAO]
  lazy val productDao = wire[ProductDAO]
  lazy val storeDao = wire[StoreDAO]
  lazy val orderDao = wire[OrderDAO]
  lazy val orderItemDao = wire[OrderItemDAO]
}
