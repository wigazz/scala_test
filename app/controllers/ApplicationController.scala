package controllers

import dao.UserDAO
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext

class ApplicationController()(implicit ec: ExecutionContext) extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

}