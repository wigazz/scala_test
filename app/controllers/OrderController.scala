package controllers

import exceptions.order.OpenOrderExistsException
import models.User
import play.api.mvc.{Action, Controller}
import services.OrderService

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class OrderController(orderService: OrderService)(implicit ec: ExecutionContext) extends Controller {

  def create = Action.async { request =>
    // TODO брать юзера из реквеста
    val user = User(id = Some(1), login = "asd", password = "asd")

    orderService.create(user).map({
      case Success(_) => Created
      case Failure(e) => e match {
        case _: OpenOrderExistsException => Conflict
        case _ => InternalServerError
      }
    })
  }

}
