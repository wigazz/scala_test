package controllers

import exceptions.user.UserExistsException
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.UserService

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class UserController(userService: UserService)(implicit ec: ExecutionContext) extends Controller {

  case class UserRequest(login: String, password: String)

  object UserRequest {
    implicit val format = Json.format[UserRequest]
  }

  def register = Action.async(parse.json) { request =>
    request.body.validate[UserRequest].map  {
      case userRequest: UserRequest => userService.create(userRequest.login, userRequest.password).map({
        case Success(_) => Created
        case Failure(e) => e match {
          case _: UserExistsException => Conflict
          case _ => InternalServerError
        }
      })
    }.recoverTotal {
      e => Future(BadRequest)
    }
  }

}
