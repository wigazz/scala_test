import controllers.Assets
import modules.{ServiceModule, ControllerModule, DaoModule, DatabaseModule}
import play.api.ApplicationLoader.Context
import play.api._
import play.api.routing.Router
import router.Routes
import com.softwaremill.macwire._

import scala.concurrent.ExecutionContext

class AppApplicationLoader extends ApplicationLoader {
  def load(context: Context): Application = {
    (new BuiltInComponentsFromContext(context) with AppComponents).application
  }
}

trait AppComponents
  extends BuiltInComponents
  with DatabaseModule
  with DaoModule
  with ServiceModule
  with ControllerModule
{
  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  lazy val assets: Assets = wire[Assets]
  lazy val prefix: String = "/"
  lazy val router: Router = {
    lazy val prefix = "/"
    wire[Routes]
  }
}