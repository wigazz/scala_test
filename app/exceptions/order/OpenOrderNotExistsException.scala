package exceptions.order

class OpenOrderNotExistsException (message: Option[String] = Some("Open order not exists"), cause: Option[Throwable] = None) extends Exception

object OpenOrderNotExistsException {

  def apply() = new OpenOrderNotExistsException()

}
