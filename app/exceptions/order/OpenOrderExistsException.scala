package exceptions.order

class OpenOrderExistsException (message: Option[String] = Some("Open order exists"), cause: Option[Throwable] = None) extends Exception

object OpenOrderExistsException {

  def apply() = new OpenOrderExistsException()

}
