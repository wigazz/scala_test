package exceptions.store

class StoreNotFoundException (message: Option[String] = Some("Store not found"), cause: Option[Throwable] = None) extends Exception

object StoreNotFoundException {

  def apply() = new StoreNotFoundException

}
