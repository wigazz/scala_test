package exceptions.user

class UserExistsException(message: Option[String] = Some("User exists"), cause: Option[Throwable] = None) extends Exception

object UserExistsException {

  def apply() = new UserExistsException

}
