package services

import dao.UserDAO
import exceptions.user.UserExistsException
import models.User
import org.mindrot.jbcrypt.BCrypt
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class UserService(db: Database, userDAO: UserDAO)(implicit ec: ExecutionContext) {

    def create(login: String, password: String): Future[Try[User]] = {
      db.run(
        (for {
          userOption <- userDAO.findByLogin(login)
          user <- userOption match {
            case None => userDAO.insert(User(login = login, password = BCrypt.hashpw(password, BCrypt.gensalt())))
            case Some(user) => DBIO.failed(new UserExistsException)
          }
        } yield user).transactionally.asTry
      )
//      db.run(userDAO.findByLogin(login)).flatMap({
//        case None =>
//      })
//        .flatMap({
//        case None => userDAO.insert(User(login = login, password = BCrypt.hashpw(password, BCrypt.gensalt()))).map(Right(_))
//        case _ => Future(Left(new UserExistsException))
//      })
    }

}
