package services

import dao.{ProductDAO, StoreDAO}
import exceptions.store.StoreNotFoundException
import models.{Store, StoreWithProducts}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class StoreService(db: Database, storeDAO: StoreDAO, productDAO: ProductDAO)(implicit ec: ExecutionContext) {

  def getByIdWithProducts(storeId: Int): Future[Try[StoreWithProducts]] = {
    db.run(
      (for {
        storeOption <- storeDAO.findById(storeId)
        storeWithProducts <- storeOption match {
          case None => DBIO.failed(new StoreNotFoundException)
          case Some(store) => productDAO.findByStoreId(store.id.get).map(
            products => StoreWithProducts.create(store, products)
          )
        }
      } yield storeWithProducts).transactionally.asTry
    )

  }

}
