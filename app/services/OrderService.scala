package services

import dao.{OrderDAO, OrderItemDAO}
import exceptions.order.{OpenOrderNotExistsException, OpenOrderExistsException}
import models.{Order, OrderItem, User}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Try, Failure}

class OrderService(
  db: Database,
  orderDAO: OrderDAO,
  orderItemDAO: OrderItemDAO
)(implicit ec: ExecutionContext) {

  def create(user: User): Future[Try[Order]] = {

    db.run(
      (for{
        orderOption <- orderDAO.findOpenOrderByUserId(user.id.get)
        order <- orderOption match {
          case Some(_) => DBIO.failed(OpenOrderExistsException())
          case None => orderDAO.insert(Order(userId = user.id.get, status = "open"))
        }
      } yield order).transactionally.asTry
    )

  }

  def addItem(user: User, product: models.Product, quantity: Int): Future[Try[OrderItem]] = {

      db.run(
        (for{
          orderOption <- orderDAO.findOpenOrderByUserId(user.id.get)
          orderItemOption <- orderOption match {
            case None => DBIO.failed(OpenOrderNotExistsException())
            case Some(order) => orderItemDAO.findByOrderIdAndProductId(order.id.get, product.id.get)
          }
          orderItem <- orderItemOption match {
            case Some(orderItem) =>
              orderItemDAO.updateQuantity(orderItem, orderItem.quantity + quantity)
              DBIO.successful(orderItem)
            case _ => orderItemDAO.insert(OrderItem(None, orderOption.get.id.get, product.id.get, quantity))
          }
        } yield orderItem).transactionally.asTry
      )
  }

}
