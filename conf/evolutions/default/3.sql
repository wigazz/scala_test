# Products schema

# --- !Ups

CREATE TABLE tbl_product
(
    id BIGSERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,
    quantity INT NOT NULL CHECK (quantity > 0),
    price NUMERIC(11, 2) NOT NULL CHECK (price > 0),
    store_id BIGINT NOT NULL REFERENCES tbl_store (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

# --- !Downs
