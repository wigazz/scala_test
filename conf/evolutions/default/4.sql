# Orders schema

# --- !Ups

CREATE TABLE tbl_order
(
    id BIGSERIAL PRIMARY KEY,
    status VARCHAR(100) NOT NULL,
    user_id BIGINT NOT NULL REFERENCES tbl_user (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

# --- !Downs
