# Users schema

# --- !Ups

CREATE TABLE tbl_user
(
    id BIGSERIAL PRIMARY KEY,
    login varchar(100) NOT NULL UNIQUE,
    password varchar(100) NOT NULL
);

# --- !Downs
