# OrderItems schema  OrderItem(id: Option[Int] = None, orderId: Int, productId: Int, quantity: Int)

# --- !Ups

CREATE TABLE tbl_order_item
(
    id BIGSERIAL PRIMARY KEY,
    order_id BIGINT NOT NULL REFERENCES tbl_order (id) ON UPDATE CASCADE ON DELETE RESTRICT,
    product_id BIGINT NOT NULL REFERENCES tbl_product (id) ON UPDATE CASCADE ON DELETE RESTRICT,
    quantity INT NOT NULL CHECK ( quantity > 0 ),
    UNIQUE (order_id, product_id)
);

# --- !Downs
